<?php

if (!function_exists("_recaptcha_qsencode")) {
    include path::plugins("recaptchalib.php");
}

class recaptcha extends app {

    public static function form() {
        ob_start();
        $publickey = ""; // you got this from the signup page
        echo recaptcha_get_html($publickey);
        return ob_get_clean();
    }

    public static function validacao() {
        $privatekey = "";
        $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
        return $resp->is_valid;
    }

}

?>